/* globals Chart:false, feather:false */

(function () {
  'use strict'

  feather.replace()

})();

var fieldJson;

$(document).ready(function(){
  $("#loader").fadeOut(1500);
  // Variable Defination
  const userForm = $("#user-data-form");
  const fieldCustomizationForm = $("#adding-input-field");


  ConstructUserInformationForm(userForm);

  const selectFeild = $("#FeildType");
  selectFeild.on("change", function(event){
    if( event.target.value == "select" ){
        $("#select-box-option-container").removeClass("d-none");
        $("#select-box-option-container #SelectOption").prop("required", true)
    }else {
      $("#select-box-option-container").addClass("d-none");
      $("#select-box-option-container #SelectOption").prop("required", false);
      $("#select-box-option-container #SelectOption").val("");
    }
  });



});

function ConstructFormFeild(label, name, type, isRequired){
  return `
    <div class="form-group">
        <label for="${name}">${label}</label>
        <input type="${type}" class="form-control" name="${name}" id="${name}" aria-describedby="nameHelp" required="${isRequired}">
    </div>  
`;
}

function ConstructSelectFeild(label, name, values, isRequired){
  var option = "";
  values.forEach(function(data) {
   option = option + `<option value="${data}">${data}</option>`;
  });
  return `
    <div class="form-group">
        <label for="${name}">${label}</label>
        <select class="form-control" name="${name}" id="${name}" required="${isRequired}">
            <option value="----" selected>Select One Feild</option>
            ${
             option
            }
        </select>
    </div>  
`;
}

function ConstructUserInformationForm(formSelector){

  $.getJSON('/api/fields', function (result){
    previousContent = formSelector.html();
    formSelector.html("");
    result.data.forEach(function(data){
      if(data.values){
        formSelector.append(ConstructSelectFeild(data.label, data.name, data.values, data.required))
      }else {
        formSelector.append(ConstructFormFeild(data.label, data.name, data.type, data.required));
      }
    });
    formSelector.append(previousContent);
  });
}


function deleteMe(element,fieldJson){
  console.log(fieldJson);
  fieldJson.
  console.log($(element).parent())
  $(element).parent().remove();
}

function routing(){
  var href = window.location.pathname;
  console.log(href);
  $(`a[href="${href}"]`).addClass('active');
}
routing()
