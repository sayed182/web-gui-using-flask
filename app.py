import os
from flask import Flask, jsonify, request, session,flash
from flask_cors import CORS
from flask import render_template, redirect, url_for, send_from_directory, send_file
from prettytable import PrettyTable
from werkzeug.utils import secure_filename

import helper


UPLOAD_FOLDER = './templates/images/'
app = Flask(__name__, static_folder='./templates', static_url_path='/')

app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 0
app.config["SECRET_KEY"] = "OCML3BRawWEUeaxcuKHLpw"
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER


@app.context_processor
def pre_render():
    data = helper.get_json_by_name("config")
    branding = "Default Brand"
    if data.__contains__("branding"):
        branding = data["branding"]
    return dict(branding=data["branding"])


@app.route("/")
def index():
    hrm_mapped = helper.get_json_by_name('sap_success_factor')
    if not session.get('isAuthenticated') is None:
        if (len(hrm_mapped) <= 0):
            return render_template("dashboard.html", error="HRM Fields Are Not Mapped. Please Mape those to proceed")
        return render_template("dashboard.html")
    else:
        if (len(hrm_mapped) <= 0):
            return render_template("index.html", error="HRM Fields Are Not Mapped. Please Mape those to proceed")
        return render_template("index.html")


@app.route("/add-field")
def add_fields():
    fields = helper.get_json_by_name("fields")
    # print (fields)
    return render_template("add-fields.html", fields=fields)


@app.route("/add-hrm")
def add_hrm_fields():
    return render_template("add-erp-fields.html")


@app.route("/map-erp")
def map_hrm_fields():
    fields = helper.get_json_by_name("sap_success_factor")
    return render_template("map-hrm.html", fields=fields)


@app.route("/settings", methods=['GET', 'POST'])
def settings():
    if request.method == 'GET':
        data = helper.get_json_by_name("config")
        return render_template("settings.html", data=data)
    if request.method == 'POST':
        response = request.form.to_dict()
        helper.settings(response)
        return redirect('/settings')


@app.route("/login")
def login():
    if session.get('isAuthenticated') is None:
        return render_template("login.html")
    else:
        return redirect('/')


@app.route("/login", methods=['POST'])
def handle_login():
    data = request.get_json()
    if (data['name'] == "admin" and data['password'] == "password"):
        session["isAuthenticated"] = True
        return jsonify({"message": "Successfully Edited", "status": 200})
    else:
        return jsonify({"message": "Login Failed", "status": 404})

@app.route('/download/<filename>', methods=['GET'])
def download(filename):
    if not session.get('isAuthenticated') is None:
        if not os.path.exists('./'+filename):
            return "File Not Found"
        return send_from_directory(directory='./', filename=filename)
    else:
        return "Function Available Only to admin"


@app.route('/upload', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        if 'brand' not in request.files:
            flash('No file part')
            return "No file"
        file = request.files['brand']

        if file.filename == '':
            flash('No selected file')
            return "No File Empty"
        if file and helper.allowed_file(file.filename):
            filename = file.filename
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
    return "Done"

@app.route("/logout")
def logout():
    session.pop("isAuthenticated", None)
    return redirect('/')





## --------- API ROUTES DEFINED UNDER --------- ##

@app.route('/api/fields', methods=['GET', 'POST'])
def fields():
    if request.method == 'GET':
        fields = helper.get_current_fields()
        response_object = {'status': 'success', "data": fields}
        return jsonify(response_object)
    if request.method == 'POST':
        post_data = request.get_json()
        message = helper.save_fields(post_data)
        return jsonify({"message": message})


@app.route('/api/sap', methods=['GET', 'POST'])
def sap():
    if request.method == 'GET':
        fields = helper.get_current_fields()
        response_object = {'status': 'success', "data": fields}
        return jsonify(response_object)
    if request.method == 'POST':
        post_data = request.get_json()
        print(post_data)
        helper.save_sap_success_factor(post_data)
        return jsonify({"message": "Successfully Edited"})


@app.route('/postHRM', methods=['POST'])
def postHRM():
    if request.method == 'POST':
        saveOption = request.form.get('saveOption')
        data = request.form.to_dict()
        if saveOption == "database-erp":
            helper.write_csv(data)
            message = helper.post_to_HRM(data)
            return jsonify(message)
        elif saveOption == "database":
            message = helper.write_csv(data)
            return jsonify({"httpCode": 200, "message": message })
        else:
            message = "Error"
        return jsonify("Sorry Could Not Process request.")


@app.route('/api/getJson/<filename>')
def get_json_by_filename(filename):
    fields = helper.get_json_by_name(filename)
    print(fields)
    return jsonify(fields)

@app.route('/api/postJson/<filename>', methods=['POST'])
def save_json_by_filename(filename):
    data = request.get_json()
    response = helper.save_json_by_name(filename, data)
    print(data)
    return jsonify(response)

@app.route('/api/clearSapMappings', methods=['POST'])
def clear_sap_mappings():
    helper.save_sap_success_factor("")
    return jsonify({"message": "Successfully Cleared All Mappings"})


@app.route("/api/renderCSV")
def renderCSV():
    # try:
    a = open("temperature-record.csv", 'r')
    a = a.read().splitlines()
    l1 = a[0]
    l1 = l1.split(',')
    header = []
    for i in range(0, len(l1)):
        header.append(l1[i])
    table = PrettyTable(l1)

    # Adding the data
    for i in range(1, len(a)):
        if(len(a[i])>0):
            table.add_row(a[i].split(','))

    code = table.get_html_string()
    print(code)
    return jsonify({"message": "success", "status": 200, "data": code})
    # except:
    #     return jsonify({"message": "something went wrong", "status": 404, "data": None})



# ---------- Instantiating Main ---------#


if __name__ == "__main__":
    app.run(debug=True)
